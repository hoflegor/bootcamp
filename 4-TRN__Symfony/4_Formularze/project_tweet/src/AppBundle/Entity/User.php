<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * User
 *
 * @ORM\Table(name="user")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UserRepository")
 */


class User
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Tweet")
     * @ORM\JoinColumn(name="tweet_id", referencedColumnName="id")
     */
    private $tweet;
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nick", type="string", length=40, unique=true)
     */
    private $nick;

    /**
     * @var int
     *
     * @ORM\Column(name="tweet_id", type="integer", unique=true)
     */
    private $tweetId;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nick
     *
     * @param string $nick
     *
     * @return User
     */
    public function setNick($nick)
    {
        $this->nick = $nick;

        return $this;
    }

    /**
     * Get nick
     *
     * @return string
     */
    public function getNick()
    {
        return $this->nick;
    }

    /**
     * Set tweetId
     *
     * @param integer $tweetId
     *
     * @return User
     */
    public function setTweetId($tweetId)
    {
        $this->tweetId = $tweetId;

        return $this;
    }

    /**
     * Get tweetId
     *
     * @return int
     */
    public function getTweetId()
    {
        return $this->tweetId;
    }

    /**
     * Get tweet
     *
     * @return integer
     */
    public function getTweet()
    {
        return $this->tweet;
    }
}
