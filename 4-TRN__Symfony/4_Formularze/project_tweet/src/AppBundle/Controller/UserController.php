<?php

namespace AppBundle\Controller;

use AppBundle\Entity\User;
use Doctrine\DBAL\Types\TextType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class UserController extends Controller
{
    /**
     * @Route("/create")
     */
    public function createAction()
    {
        return $this->render('AppBundle:User:create.html.twig', array(
            // ...
        ));
    }

    /**
     * @Route("/newUser", name="newUser")
     */
    public function newAction()
    {

        $user = new User();

        $form = $this->createFormBuilder($user)
            ->add('nick', 'text', ['label'=>'Utwórz nick użytkownika'])
            ->add('tweet', 'entity', ['class'=>'AppBundle:Tweet', 'property'=>'content'])
            ->add('save', 'submit', ['label'=>'Zapisz'])
            ->getForm();



        return $this->render('AppBundle:User:new.html.twig', array(
            'form'=>$form->createView()
        ));
    }

    /**
     * @Route("/showAll")
     */
    public function showAllAction()
    {
        return $this->render('AppBundle:User:show_all.html.twig', array(
            // ...
        ));
    }

}
