<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Tweet;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

class TweetController extends Controller
{

    /**
     * @Route("/new", name="newTweet")
     */
    public function newAction()
    {

        $tweet = new Tweet();

        $form = $this->createFormBuilder($tweet)
            ->setAction($this->generateUrl('createTweet'))
            ->add('name', 'text', ['label' => 'Nazwa tweeta'])
            ->add('content', 'textarea', ['label' => 'Tweet'])
            ->add('save', 'submit', ['label' => 'Dodaj tweeta'], ['attr' => ['class' => "btn btn-dark"]])
            ->getForm();

        return $this->render('AppBundle:Tweet:create.html.twig', ['form' => $form->createView()]);
    }


    /**
     * @Route("/create", name="createTweet")
     */
    public function createAction(Request $request)
    {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $tweet = new Tweet();

            $form = $this->createFormBuilder($tweet)
                ->add('name', 'text', ['label' => 'Nazwa tweeta'])
                ->add('content', 'textarea')
                ->add('save', 'submit', ['label' => 'Dodaj tweeta'])
                ->getForm();

            $form->handleRequest($request);

            if ($form->isSubmitted()) {
                $form->getData();
                $em = $this->getDoctrine()->getManager();
                $em->persist($tweet);
                $em->flush();

                return $this->redirect($this->generateUrl('showAll'));

            }
        } else {
            return new Response ('Request method should be POST');
        }
    }


    /**
     * @Route("/showAll", name="showAll")
     */
    public function showAllAction()
    {
        $repo = $this->getDoctrine()->getRepository('AppBundle:Tweet');

        $tweets = $repo->findAll();

//        return new Response(var_dump($tweets));

        return $this->render(
            'AppBundle:Tweet:show_all.html.twig',
            array(
                'tweets' => $tweets,
            )
        );
    }

    /**
     * @Route("/update/{id}", name="update")
     */
    public function updateAction(Request $request, $id)
    {

        $repo = $this->getDoctrine()->getRepository('AppBundle:Tweet');
        $tweet = $repo->find($id);

        $form = $this->createFormBuilder($tweet)
            ->setAction($this->generateUrl('update', array('id' => $id)))
            ->setMethod('POST')
            ->add('name', 'text')
            ->add('content', 'text')
            ->add('save', 'submit', array('label' => 'Aktualizuj Tweeta'))
            ->getForm();

        if ($_SERVER['REQUEST_METHOD'] === 'GET') {


            return $this->render(
                'AppBundle:Tweet:update_tweet.html.twig',
                ['tweet' => $tweet, 'form' => $form->createView()]
            );

        } elseif ($_SERVER['REQUEST_METHOD'] === 'POST') {

            $form->handleRequest($request);
            if ($form->isSubmitted()) {
                $tweet = $form->getData();
                $em = $this->getDoctrine()->getManager();
                $em->persist($tweet);
                $em->flush();

                return new Response("Tweet zaktualizowany");
            }

        }

    }

}
